from django.contrib import admin
from api.models import orders,Products,ordersItems

# Register your models here.
admin.site.register(Products)
admin.site.register(orders)
admin.site.register(ordersItems)
