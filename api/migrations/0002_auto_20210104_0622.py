# Generated by Django 3.1.4 on 2021-01-04 06:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orders',
            name='mode_of_payment',
            field=models.CharField(choices=[('cash', 'cash'), ('phonpay', 'phonepay'), ('paytm', 'paytm'), ('card', 'card')], max_length=20),
        ),
        migrations.AlterField(
            model_name='orders',
            name='status',
            field=models.CharField(choices=[('new', 'new'), ('paid', 'paid')], max_length=20),
        ),
    ]
