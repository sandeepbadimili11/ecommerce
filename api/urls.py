from rest_framework import routers
from .api import RestViewSet, ProductViewSet


router=routers.DefaultRouter()
router.register('orders', RestViewSet, 'api')
router.register('products',ProductViewSet, 'api')

urlpatterns =router.urls
