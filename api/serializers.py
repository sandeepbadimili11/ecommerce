from rest_framework import serializers

from api.models import orders,ordersItems,Products


class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = orders
        fields = ('__all__')


class Orders_itemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ordersItems
        fields = ('__all__')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('__all__')