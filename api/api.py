from api.models import orders,Products
from rest_framework import viewsets,permissions
from api.serializers import OrdersSerializer,ProductSerializer

#Rest Viewset

class RestViewSet(viewsets.ModelViewSet):
    queryset=orders.objects.all()
    permissions_classes=[
        permissions.AllowAny
    ]
    serializer_class= OrdersSerializer

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    permissions_classes=[
        permissions.AllowAny
    ]
    serializer_class = ProductSerializer
