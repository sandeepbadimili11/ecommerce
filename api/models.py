from django.contrib.auth.models import User
from django.db import models
                                                                                                                 
# Create your models here.

class Products(models.Model):
    Title = models.CharField(max_length=40)
    Description = models.TextField(max_length = 100)
    image = models.ImageField(upload_to= 'templates')
    price = models.IntegerField()
    Created_At = models.DateTimeField(auto_now_add = True)
    Updated_At = models.DateField(auto_now_add=True)

PAYMENT_CHOICES = (
        ("cash", "cash"),
        ("phonpay", "phonepay"),
        ("paytm", "paytm"),
        ("card", "card"),
)
STATUS_CHOICES = (
        ("new", "new"),
        ("paid", "paid"),
)

class orders(models.Model):

    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    Total = models.IntegerField()
    Created_At = models.DateTimeField(auto_now_add = True)
    Updated_At = models.DateField(auto_now_add = True)
    status = models.CharField(max_length=20,choices=STATUS_CHOICES)
    mode_of_payment = models.CharField(max_length=20,choices=PAYMENT_CHOICES)
    def __str__(self):
        return str(self.user_id)

class ordersItems(models.Model):
    orderId = models.ForeignKey(orders,on_delete=models.CASCADE)
    productId = models.ForeignKey(Products,on_delete=models.CASCADE)
    Quantity = models.IntegerField()
    price = models.IntegerField()

    def __str__(self):
        return str(self.product_id)
